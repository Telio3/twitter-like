import path from 'path';
import conf from './conf.config';

export default {
    dbUrl: conf.DB_LINK,
    cert: path.join(__dirname, '../../ssl/local.crt'),
    key: path.join(__dirname, '../../ssl/local.key'),
    portHttp: 3000,
    portHttps: 3001,
};
