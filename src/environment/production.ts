import conf from './conf.config';

export default {
    dbUrl: conf.DB_LINK,
    cert: conf.CERT,
    key: conf.KEY,
    portHttp: 80,
    portHttps: 443,
};
